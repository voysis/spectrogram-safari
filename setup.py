import sys
from setuptools import find_packages
from setuptools import setup

required = [
    "Click==7.0",
    "Flask==1.1.1",
    "Jinja2==2.10.1",
    "MarkupSafe==1.1.1",
    "Werkzeug==0.15.5",
    "appdirs==1.4.3",
    "attrs==19.1.0",
    "black==19.3b0",
    "itsdangerous==1.1.0",
    "toml==0.10.0",
    "versioneer==0.18",
]

needs_flake8 = {"flake8"}.intersection(sys.argv)
flake8 = ["flake8==3.5.0"] if needs_flake8 else []

setup(
    name="spectrogram_safari",
    author="Mark Gaynor",
    author_email="mgaynor@voysis.com",
    python_requires='>3.6.0',
    url="https://bitbucket.org/voysis/spectrogram_safari",
    description="spectrogram_safari",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    packages=find_packages(exclude=["*tests*"]),
    license="MIT",
    install_requires=required,
    setup_requires=flake8,
    entry_points={"console_scripts": ["spectrogram_safari = spectrogram_safari.main:cli"]},
)

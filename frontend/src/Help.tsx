import React from 'react';
import './App.css';

export const HelpComponent: React.FC = () => {
  return (
    <div className="linguist-lizzy-wrap">
      <div className="instruction-text">
        <h1>Welcome to Voysis’ Spectacular Spectrogram Safari Game!</h1>
        <p>I’m Linguist Lizzy, your guide through the wild woods of phonetics!</p>
        <p>
          Before we get started on our quest to spot the most exotic and fantastic sounds English has to offer, I’ll need to explain a few things.
        </p>
        <p>
          First off, the study of speech sounds of a language is called Phonetics. An individual sound is called a Phone. Today, we’ll be Phone-Hunting! So pull out your wit and your glasses and let’s take a quick peek at what kind of exotic sounds we’ll be looking for!
        </p>
        <p>
          We’ll be breaking down the types of phones we’re looking for into two classes: Consonants, and Vowels. We’ll further break down the Consonants into two classes: Voiced Consonants and Voiceless Consonants.
        </p>
        <p>
          You’re probably already familiar with Consonants and Vowels as a concept, so we’ll start there. These fabulous Phone Sets may seem less exotic, but I assure you, they are just as astounding as the less commonly known voiced and voiceless sounds.
        </p>
        <p>
          A consonant is a gritty phone that can take many shapes, but it’s defining feature is that the air flow that pushes the sound through the vocal tract and out into the bright, wonderful world, is obstructed.
        </p>
        <p>
          To feel this in action, say the word “love”. Note how the tip of your tongue touches the roof of your mouth. This causes an obstruction as the air flows through.
        </p>
        <p>
          Vowels are the opposite. They have no obstruction. Say the word “love” again, but now notice that when you make the “o” sound, your tongue lays flat and there is no obstruction as the air flows.
        </p>
        <p>
          That is the basic knowledge you will need to spot those sassy little Consonants and Vowels.
        </p>
        <p>
          To spot Voiced and Voiceless Consonants, we’ll need to try a different exercise. Say “Zzzzzzzzzzzzzzzzzzzzzzzzzz” like a bee buzzing around your head.  But when you do it, hold two fingers to your throat. Can you feel the movement? That’s your vocal folds vibrating! That means you have a wild and wonky Voiced Consonant on your hands.
        </p>
        <p>
          In contrast, say “Shhhhhhhhhhhhhhhhhhhhhhhh” as if you’re shushing someone in the movie theater, and keep your fingers on your throat. Did you feel that? No movement! That’s because your vocal folds aren’t vibrating when you make this “sh” sound, and that means we’ve spotted a stealthy Voiceless Consonant!
        </p>
        <p>
          Now, let’s look at some real-life examples! We’ll be looking at spectrograms, which is a picture of your speech. Yes, we can actually take pictures of speech! How amazing is that?
        </p>
        <p>
          With these pictures, your objective is to discover, through your resourcefulness and wit, what the person from the picture is saying!
        </p>
        <p>
          Here’s what you want to look for as you take your Safari through these Spectacular Spectrograms.
        </p>
        <p>
          Vowels are long and bright red, because they have oodles of energy! See the vowel “a” in the word “cave” shown below. Also, notice that even though the spelling of “cave” has two vowels, the word only has one vowel! Holy cow! The “e” isn’t just silent, it’s not there at all!
        </p>
        <p><img src="/images/vowel_drawing_cave.jpg"/></p>
        <p>Here’s a more complicated, but no less mesmerizing, example of a vowel (two even!) in the word “fairy”. The first is an “a” vowel and the second is an “i” vowel:</p>
        <p><img src="/images/vowel_drawing_fairy.jpg"/></p>
        <p>We can see two, high-energy bright spots. But wait, what’s that dip in between them? That’s the “r” ! The “r” looks a bit like a vowel, but while the vowels are mostly straight shots of red across the spectrogram, an “r” will dip, like in this example.</p>
        <p>In contrast, consonants are a bit more sly and a lot less loud. Here’s “cave” again, but with the consonants denoted</p>
        <p><img src="/images/consonant_drawing_cave.jpg"/></p>
        <p>And here’s “fairy”:</p>
        <p><img src="/images/consonant_drawing_fairy.jpg"/></p>
        <p>
          So, you’ve got a basic understanding of how to hunt down vowels and consonants. Amazing! “What about Voiced and Voiceless Consonants” you say?
        </p>
        <p>
          Well, these are much easier to identify. Whether a consonant is voiced or not, is shown by the very bottom of the spectrogram. If it’s red hot like fire, with lots and lots of energy, then it’s voiced! If not, then it’s voiceless.
        </p>
        <p><b>Expert Adventurer Tip</b>: vowels are always voiced!</p>
        <p>Here is the word “zeal”, with the “z” highlighted. “Z” is a Voiced Consonant.</p>
        <p><img src="/images/voiced_drawing_zeal.jpg"/></p>
        <p>See all that energy? It’s smoking hot! We could burn down the jungle with that fire (but let’s not …)!</p>
        <p>Now check out the word “seal”, with the “s” highlighted. “S” is a Voiceless Phone:</p>
        <p><img src="/images/voiceless_drawing_seal.jpg"/></p>
        <p>
          See how cool and calm the bottom is? No danger of a jungle fire here!
        </p>
        <p>
          Here’s a few more examples for good measure, so we know exactly what we’re looking for in this Spectacular Safari Adventure!
        </p>
        <p>The “a” in “Pat”:</p>
        <p><img src="/images/vowel_drawing_pat.jpg"/></p>
        <p>The “i” in “Pit”:</p>
        <p><img src="/images/vowel_drawing_pit.jpg"/></p>
        <p>The Voiced Consonant “v” in “Very”:</p>
        <p><img src="/images/voiceless_drawing_very.jpg"/></p>
        <p>The Voiceless Consonant “f” in “Fairy”:</p>
        <p><img src="/images/voiceless_drawing_fairy.jpg"/></p>
        <p>
          Now, armed with all that information, are you ready to start you Safari? If you want more in-depth details about these magical and mysterious Phone creatures, you can click into the “Expert Adventurer” page. If you’re ready to get started, just click “Start the Safari”.
        </p>
        <p>Let’s Go!</p>
      </div>
    </div>
  );
}


import React from 'react';
import './App.css';
import { Question } from './Types';
import { concat } from 'ramda';
import { shuffle } from './utils'

interface props {
  question: Question
  answerQuestion: (answer: string, question: Question) => void
}


export const QuestionComponent: React.FC<props> = (props) => {
  const options = shuffle(concat(props.question.choices, [props.question.correct_word])) as string[]
  return (
    <div>
      <div>
        <img src={props.question.spectrogram_route}/>
      </div>
      <div className="answer-buttons">
        {options.map((value, index) => {
          return <button className="answer-button" onClick={() => props.answerQuestion(value, props.question)}>{value}</button>
        })}
      </div>
    </div>
  );
}


import React, { Component } from 'react';
import './App.css';
import { Question } from './Types';
import axios from 'axios'
import { QuestionComponent } from './Question';
import { isEmpty } from 'ramda'
import { HelpComponent } from './Help';

function playWrong(){
  let audio = new Audio();
  audio.src = "wrong.wav";
  audio.load();
  audio.play();
}
function playCorrect(){
  let audio = new Audio();
  audio.src = "correct.mp3";
  audio.load();
  audio.play();
}

interface State {
  questions: Question[]
  score: number
  currentQuestion: number
  questionAttempted: boolean
  showHelp: boolean
}
class App extends Component<{}, State> {
  private data: Question[] = []
  constructor(props: any){
    super(props)
    this.getQuestions()
    this.state = {
      questions: [],
      score: 0,
      currentQuestion: 0,
      questionAttempted: false,
      showHelp: false
    }
    this.answerQuestion = this.answerQuestion.bind(this)
    this.setScore = this.setScore.bind(this)
    this.nextQuestion = this.nextQuestion.bind(this)
    this.toggleHelp = this.toggleHelp.bind(this)
    // this.answerQuestion = this.answerQuestion.bind(this)
  }
  public render() {
    console.log(this.state.questions)
    if(this.state.showHelp){
      return (
        <div className="nav-bar">
          <h2>
            <a onClick={this.toggleHelp}>Help</a>
          </h2>
          <HelpComponent />
        </div>
      )
    } else {
      return (
        <div>
          <div className="nav-bar">
            <h2>
              <a onClick={this.toggleHelp}>Help</a>
            </h2>
          </div>
          <div className="linguist-lizzy-wrap">
            <div className="game-container">
              <h2 className="score-text">Your score is: {this.state.score}</h2>
              {this.showQuestion(this.state.currentQuestion)}
              {this.state.questionAttempted &&
                <div className="pass-button">
                  <button className="answer-button" onClick={this.nextQuestion}>Pass</button>
                </div>
              }
            </div>
          </div>
        </div>
      )
    }
  }
  private showQuestion(question: number) {
    console.log(isEmpty(this.state.questions))
    if(this.state.questions.length > question && !isEmpty(this.state.questions)) {
      return <QuestionComponent question={this.state.questions[question]} answerQuestion={this.answerQuestion} />
    } else {
      return <div/>
    }
  }
  private nextQuestion() {
    this.setState({
      currentQuestion: this.state.currentQuestion+1,
      questionAttempted: false
    })
  }
  private toggleHelp() {
    this.setState({
      showHelp: !this.state.showHelp
    })
  }
  private answerQuestion(answer: string, question: Question) {
    if(answer === question.correct_word) {
      playCorrect()
      this.setScore(1)
      this.nextQuestion()
    } else {
      playWrong()
      this.setState({
        questionAttempted: true
      })
      this.setScore(-1)
    }
  }
  private setScore(addend: number) {
    this.setState({
      score: this.state.score + addend
    })
  }
  private async getQuestions() {
    const response = await axios.get('/data.html')
    this.setState({
      questions: response.data.data
    })
  }
}

export default App;

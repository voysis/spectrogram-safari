export interface Question {
  round: number
  spectrogram_route: string
  correct_word: string
  choices: string[]
}
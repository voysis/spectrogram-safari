from collections import defaultdict
import json
import os
import random
import subprocess
import logging

random.seed(1)

logger = logging.getLogger(__name__)


def load_corpus(corpus_fn: str) -> dict:
    corpus_dir = os.path.dirname(corpus_fn)

    corpus_dict = defaultdict(lambda: [], {})
    with open(corpus_fn, "r") as fin:
        for line in fin:
            word = os.path.dirname(line)
            wav_fn = os.path.join(corpus_dir, line.strip())
            corpus_dict[word].append(wav_fn)

    return corpus_dict


def generate_spectrogram(wav_fn: str, out_fn: str):
    logger.debug("Generating {}".format(out_fn))
    sox_cmd = "sox {} -n spectrogram -o {}".format(wav_fn, out_fn)

    try:
        return subprocess.check_output(sox_cmd, stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError as e:
        logger.error(e.output)
        raise


def generate(
    corpus_fn: dict,
    num_rounds: int,
    num_choices: int,
    image_dirname: str = "img",
) -> str:
    image_dir = os.path.join(os.path.dirname(__file__), image_dirname)
    os.makedirs(image_dir, exist_ok=True)
    corpus = load_corpus(corpus_fn)
    words = list(corpus.keys())

    data = []
    for round_num in range(0, num_rounds):
        correct_word = words[random.randint(0, len(corpus) - 1)]
        wav_fn = corpus.get(correct_word)[random.randint(0, len(corpus.get(correct_word)))]

        spectrogram_name = "{}.png".format(random.getrandbits(64))
        spectrogram_fn = os.path.join(image_dir, spectrogram_name)
        generate_spectrogram(wav_fn, spectrogram_fn)

        choices = []
        while len(choices) != num_choices:
            choice_word = words[random.randint(0, len(corpus) - 1)]
            if choice_word not in choices and choice_word != correct_word:
                choices.append(choice_word)

        data.append({
            "round": round_num,
            "wav_fn": wav_fn,
            "spectrogram_route": os.path.join(image_dirname, spectrogram_name),
            "correct_word": correct_word,
            "choices": choices,
        })

    return json.dumps({"data": data})

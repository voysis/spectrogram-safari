import configparser
import logging
import os

from flask import Flask, send_from_directory

from spectrogram_safari import data

app = Flask(__name__, static_folder="static/static")


@app.route("/data.html")
def play(config: str = "configs/config.ini"):
    logging.basicConfig(format="%(asctime)s %(levelname)-8s %(message)s", level="INFO")
    logger = logging.getLogger(__name__)

    logger.debug("Reading {}".format(config))
    config_parser = configparser.ConfigParser()
    config_parser.read(config)
    corpus_fn = config_parser["DEFAULT"]["corpus"]
    num_rounds = config_parser["DEFAULT"].getint("num_rounds")
    num_choices = config_parser["DEFAULT"].getint("num_choices")

    return data.generate(corpus_fn, num_rounds, num_choices)


@app.route("/img/<path:filename>", methods=["GET"])
def get_img(filename):
    return send_from_directory(
        os.path.join(os.path.abspath(os.path.dirname(__file__)), "img"), filename
    )

@app.route("/", methods=["GET"])
@app.route("/<path:filename>", methods=["GET"])
def root(filename: str = "index.html"):
    root = os.path.join(os.path.abspath(os.path.dirname(__file__)), "static")
    return send_from_directory(
        root , filename
    )


if __name__ == "__main__":
    app.run()

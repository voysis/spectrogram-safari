# SPECTROGRAM SAFARI

![Logo](logo.png)

## Frontend

```
cd frontend
npm run build
cd ..
cp -r frontend/build/* spectogram-safari/static
```

## Flask App

## Sox 

For Ubuntu 

```
sudo apt-get install sox
```

For Frank

```
module load sox-14.4.2-gcc-4.8.5-kceo62n
```

## Python

```
python3.6 -m venv .venv
source .venv/bin/activate
pip install -e .
```

## Data
```
mkdir -p data/speech-commands
(cd data/speech-commands && wget http://download.tensorflow.org/data/speech_commands_v0.01.tar.gz && tar xzf speech_commands_v0.01.tar.gz && rm speech_commands_v0.01.tar.gz)
```


## Run app

```
pythonpath=. python -m spectrogram_safari.main
```
